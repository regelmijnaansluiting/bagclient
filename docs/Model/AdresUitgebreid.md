# AdresUitgebreid

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type_adresseerbaar_object** | [**\BagClient\Model\TypeAdresseerbaarObject**](TypeAdresseerbaarObject.md) |  | [optional] 
**adresseerbaar_object_geometrie** | [**\BagClient\Model\PuntOfVlak**](PuntOfVlak.md) |  | [optional] 
**adresseerbaar_object_status** | **string** |  | [optional] 
**gebruiksdoelen** | [**\BagClient\Model\Gebruiksdoel[]**](Gebruiksdoel.md) |  | [optional] 
**oppervlakte** | **int** |  | [optional] 
**oorspronkelijk_bouwjaar** | **string[]** |  | [optional] 
**geconstateerd** | [**\BagClient\Model\AdresUitgebreidGeconstateerd**](AdresUitgebreidGeconstateerd.md) |  | [optional] 
**inonderzoek** | [**\BagClient\Model\AdresUitgebreidInOnderzoek**](AdresUitgebreidInOnderzoek.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

