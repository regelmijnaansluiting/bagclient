# PuntOfVlak

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**punt** | [**\BagClient\Model\Point**](Point.md) |  | [optional] 
**vlak** | [**\BagClient\Model\Surface**](Surface.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

