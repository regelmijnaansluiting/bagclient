# AdresUitgebreidHalCollection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_embedded** | [**\BagClient\Model\AdresUitgebreidHalCollectionEmbedded**](AdresUitgebreidHalCollectionEmbedded.md) |  | [optional] 
**_links** | [**\BagClient\Model\HalCollectionLinks**](HalCollectionLinks.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

