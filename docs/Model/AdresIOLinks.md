# AdresIOLinks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**self** | [**\BagClient\Model\HalLink**](HalLink.md) |  | [optional] 
**openbare_ruimte** | [**\BagClient\Model\HalLink**](HalLink.md) |  | [optional] 
**nummeraanduiding** | [**\BagClient\Model\HalLink**](HalLink.md) |  | [optional] 
**woonplaats** | [**\BagClient\Model\HalLink**](HalLink.md) |  | [optional] 
**adresseerbaar_object** | [**\BagClient\Model\HalLink**](HalLink.md) |  | [optional] 
**panden** | [**\BagClient\Model\HalLink[]**](HalLink.md) | Het/de aan het adres gerelateerde pand(en). | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

